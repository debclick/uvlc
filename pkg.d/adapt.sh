#package specific information for the desktop file
sed -i "s/@name@/VLC Player/" ${INSTALL_DIR}/*.desktop
sed -i "/^Terminal=/i Icon=share/icons/hicolor/256x256/apps/vlc.png" ${INSTALL_DIR}/vlc.desktop
#a short description for the package
sed -i "s/@description@/vlc is a mediaplayer/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
